# System Memory

## What

	-System.Memory version 4.5.3

	-Provides types for efficient representation and pooling of managed, stack, and native memory segments and sequences of such segments, along with primitives to parse and format UTF-8 encoded text stored in those memory segments.

	-Commonly Used Types:
		+ System.Span
		+ System.ReadOnlySpan
		+ System.Memory
		+ System.ReadOnlyMemory
		+ System.Buffers.MemoryPool
		+ System.Buffers.ReadOnlySequence
		+ System.Buffers.Text.Utf8Parser
		+ System.Buffers.Text.Utf8Formatter

## Requirements
[![Unity 2018.3+](https://img.shields.io/badge/unity-2018.3+-brightgreen.svg?style=flat&logo=unity&cacheSeconds=2592000)](https://unity3d.com/get-unity/download/archive)
[![.NET 2.0 Scripting Runtime](https://img.shields.io/badge/.NET-2.0-blueviolet.svg?style=flat&cacheSeconds=2592000)](https://docs.unity3d.com/2018.3/Documentation/Manual/ScriptingRuntimeUpgrade.html)

## Installation

```bash
"com.yenmoc.system-memory":"https://gitlab.com/yenmoc/system-memory"
or
npm publish --registry http://localhost:4873
```

